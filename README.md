
# 百百川的红警笔记

**我是百百川**，这是我的**红警笔记**仓库!  
在做视频的时候，有很多内容太啰嗦，就不方便**做进视频里**，  
所以，跟视频相关的补充内容，我会写在这个红警笔记仓库里面。  

---


# 红警笔记 · 列表

### [【红警下载】 + 【红警问题解决办法】](https://gitee.com/gtx750ti/bbc/blob/master/%E7%BA%A2%E8%AD%A6%E4%B8%8B%E8%BD%BD.md)

### [【红警语音包】demo2测试版](https://gitee.com/gtx750ti/bbc/issues/I46WH0)  

### [【红警载入图】V0.2.1测试版](https://gitee.com/gtx750ti/bbc/issues/I4MX8A)  

### [【红警地图编辑器】下载+安装+使用教程](https://gitee.com/gtx750ti/bbc/issues/I6ZTZ1)

